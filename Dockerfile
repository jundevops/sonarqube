FROM adoptopenjdk/openjdk8:alpine-slim
EXPOSE 8080
ARG JAR_FILE=target/*.jar
RUN addgroup -S devops-security && adduser -S devsecops -G devops-security
COPY ${JAR_FILE} /home/devsecops/app.jar
USER devsecops
ENTRYPOINT ["java","-jar","/home/devsecops/app.jar"]


# # Stage 1: Build
# FROM maven:3.6.3-openjdk-8-slim AS build
# WORKDIR /workspace
# COPY . .
# RUN mvn clean package -DskipTests

# # Stage 2: Package
# FROM adoptopenjdk/openjdk8:alpine-slim
# EXPOSE 8080
# RUN addgroup -S devops-security && adduser -S devsecops -G devops-security
# COPY --from=build /workspace/target/*.jar /home/devsecops/app.jar
# USER devsecops
# ENTRYPOINT ["java","-jar","/home/devsecops/app.jar"]